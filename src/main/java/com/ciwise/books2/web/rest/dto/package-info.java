/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.ciwise.books2.web.rest.dto;
