/**
 * Data Access Objects used by WebSocket services.
 */
package com.ciwise.books2.web.websocket.dto;
