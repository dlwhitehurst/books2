'use strict';

angular.module('books2App')
    .controller('BookController', function ($scope, $state, $modal, Book, BookSearch, ParseLinks) {
      
        $scope.books = [];
        $scope.page = 0;
        $scope.loadAll = function() {
            Book.query({page: $scope.page, size: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.books = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.search = function () {
            BookSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.books = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.book = {
                title: null,
                description: null,
                publicationDate: null,
                price: null,
                id: null
            };
        };
    });
