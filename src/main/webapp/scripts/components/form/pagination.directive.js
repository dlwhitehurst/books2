/* globals $ */
'use strict';

angular.module('books2App')
    .directive('books2AppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
