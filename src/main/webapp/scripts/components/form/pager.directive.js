/* globals $ */
'use strict';

angular.module('books2App')
    .directive('books2AppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
